import {Observable} from "rxjs/Observable";
import * as webshot from 'node-webshot';
import {Subject} from "rxjs/Subject";
import * as fs from 'fs';
import * as path from 'path';
const uuidv1 = require('uuid/v1');

export class WebshotService {

    /**
     * Получаем изображение из html
     * @param {string} html
     * @returns {Observable<string>}
     */
    public getImageByHtml(html: string): Observable<string> {
        const idPng: string = uuidv1() + '.png';
        const subject: Subject<string> = new Subject();
        const pathFile: string = path.join(__dirname, idPng);
        webshot(html,  pathFile, {siteType:'html'}, (err) => {
            if (err) {
                subject.next('');
            }
            console.log('Файл ', pathFile, ' успешно создан');
            subject.next(pathFile);
            this.removeFile(pathFile)
        });
        return subject.asObservable();
    }

    /**
     * Удаление файла по названию
     * @param {string} path
     */
    private removeFile(path: string): void {
        fs.stat(path, (err, stats) => {
            if (err) {
                console.log('Ошибка удаления файла ', path);
                return;
            }
            if (stats.isFile()) {
                fs.unlink(path, (err) => {
                    if (err) {
                        console.log('Ошибка удаления файла ', path);
                    } else {
                        console.log('Файл ', path, ' успешно удален');
                    }
                    return;
                });
            }
        });
    }
}


export const webshotService: WebshotService = new WebshotService();