import {Observable} from "rxjs/Observable";
import * as jade from 'jade';

export class JadeService {

    public getStringFromJade(path: string, compileObject: Object): Observable<string> {
        return new Observable(observer => {
            jade.renderFile(path, compileObject, (err, html: string) => {
                observer.next(!err ? html : '');
                observer.complete();
            });
        });
    }

}


export const jadeService: JadeService = new JadeService();