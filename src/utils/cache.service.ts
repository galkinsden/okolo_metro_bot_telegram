export class CacheService<IKey, IElem> {

    private cache: Map<IKey, IElem> = new Map<IKey, IElem>();

    public setInCache(id: IKey, elem: IElem): this {
        this.cache.set(id, elem);
        return this;
    }

    public getFromCache(id: IKey): IElem {
        return this.cache.get(id);
    }

    public deleteFromCache(id: IKey): this {
        this.cache.delete(id);
        return this;
    }

}