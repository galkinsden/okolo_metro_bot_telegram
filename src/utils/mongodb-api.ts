import * as Rx from "rxjs";
import {config, EMongodbCollections} from "../config";
import {Observable} from "rxjs/Observable";
import {MongoClient} from 'mongodb';
import {Subject} from "rxjs/Subject";
import 'rxjs/add/operator/switchMap';

export class Mongodb {

    public constructor() {}

    private connectToDb(): Observable<any> {
        let subject: Subject<any> = new Rx.Subject();

        MongoClient.connect(config.mongodb, (err, database) => {
            if (err) {
                console.error('Ошибка подключения к mongodb!');
                subject.next(null);
                return;
            }
            subject.next(database);
        });


        return subject.asObservable();
    }

    private closeConnect(db): Observable<boolean> {

        let subject: Subject<any> = new Rx.Subject();

        db.close((err) => {
            if (err) {
                console.error('Ошибка закрытия подключения к mongodb!');
                subject.next(false);
            }
            subject.next(true);
        });


        return subject.asObservable();
    }

    public insertOne(document: any, collection: EMongodbCollections): Rx.Observable<boolean> {
        return this.connectToDb()
            .switchMap((db) => {
                let subject: Subject<any> = new Rx.Subject();

                db.collection(collection)
                    .insertOne(document, (err) => {
                        err ? subject.next(false) : subject.next(true);
                        this.closeConnect(db);
                    });

                return subject.asObservable();
            })
            .catch(() => {
                return null;
            })
    }

    public update(query: any, document: any, collection: EMongodbCollections): Rx.Observable<boolean> {
        return this.connectToDb()
            .switchMap((db) => {
                let subject: Subject<any> = new Rx.Subject();

                db.collection(collection)
                    .update(query, document, {upsert: true}, (err) => {
                        err ? subject.next(false) : subject.next(true);
                        this.closeConnect(db);
                    });

                return subject.asObservable();
            })
            .catch(() => {
                return null;
            })
    }

    public remove(query: any, collection: EMongodbCollections): Rx.Observable<boolean> {
        return this.connectToDb()
            .switchMap((db) => {
                let subject: Subject<boolean> = new Rx.Subject();

                db.collection(collection)
                    .remove(query, (err) => {
                        err ? subject.next(false) : subject.next(true);
                        this.closeConnect(db);
                    });

                return subject.asObservable();
            })
    }

    public insertMany(documents: any[], collection: EMongodbCollections): Rx.Observable<boolean> {
        return this.connectToDb()
            .switchMap((db) => {
                let subject: Subject<boolean> = new Rx.Subject();

                db.collection(collection)
                    .insertMany(documents, (err) => {
                        err ? subject.next(false) : subject.next(true);
                        this.closeConnect(db);
                    });

                return subject.asObservable();
            });
    }

    public find(query: any, collection: EMongodbCollections): Rx.Observable<any> {
        return this.connectToDb()
            .switchMap((db) => {
                let subject: Subject<any> = new Rx.Subject();
                db.collection(collection)
                    .find(query).toArray((err, result: any[]) => {
                        err ? subject.next(false) : subject.next(result);
                        this.closeConnect(db);
                    });

                return subject.asObservable();
            })
            .catch(() => {
                return null;
            })
    }

    public findOne(query: any, collection: EMongodbCollections): Rx.Observable<any> {
        return this.connectToDb()
            .switchMap((db) => {
                let subject: Subject<any> = new Rx.Subject();

                db.collection(collection)
                    .findOne(query, {},(err, result: any) => {
                        err ? subject.next(false) : subject.next(result);
                        this.closeConnect(db);
                    });

                return subject.asObservable();
            })
            .catch(() => {
                return null;
            })
    }

}