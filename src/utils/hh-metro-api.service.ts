import {Observable} from "rxjs/Observable";
import * as request from "request";
import {Subject} from "rxjs/Subject";
import {IHhMetro, IHhMetroLine, IHhMetroStation} from "../models/hh-metro.model";
import {IMetroResponse} from "../metro-core/metro-response.model";

export class HhMetroApiService {

    /**
     * Получение справочников метро с hh
     * @returns {Observable<IHhMetro>}
     */
    public getMetroJson(): Observable<IHhMetro[]> {
        let subject: Subject<IHhMetro[]> = new Subject();
        request({url: `https://api.hh.ru/metro/`, headers: {'User-Agent': 'request'}}, (error, response, body: string) => {
            let metro: IHhMetro[];
            if (error) {
                subject.next(null)
            }
            try {
                metro = JSON.parse(body);
            } catch (err) {
                metro = [];
            }
            subject.next(metro);
        });

        return subject;
    }

    public convertMetro(metro: IHhMetro): IMetroResponse[] {
        return metro && metro.lines && metro.lines.length ? metro.lines.reduce((acc: IMetroResponse[], el: IHhMetroLine) => {
            return acc.concat(el.stations.map((st: IHhMetroStation) => {
                return {
                    coordinates: [st.lng, st.lat],
                    type: 'Point',
                    id: st.id,
                    name: st.name,
                    colorLine: el.hex_color,
                    idLine: el.id,
                    nameLine: el.name,
                    nameCity: metro.name
                }
            }))
        }, []) : [];
    }

}


export const hhMetroApiService: HhMetroApiService = new HhMetroApiService();