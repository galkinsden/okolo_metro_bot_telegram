import {IMetroResponse} from "../metro-core/metro-response.model";
import {Mongodb} from "./mongodb-api";
import {EMongodbCollections} from "../config";
import {Observable} from "rxjs/Observable";

export class GeoService {

    public static DIST_DEFAULT: number = 1;

    /**
     * Поиск ближайшего метро относительно переданных координат
     * @param {number} lon
     * @param {number} lat
     * @param {number} dist
     * @returns {Observable<IMetroResponse>}
     */
    public getNearestMetro(lon: number, lat: number, dist: number = GeoService.DIST_DEFAULT): Observable<IMetroResponse> {
          return new Mongodb()
              .find({
                  coordinates: {
                      $near: [lon, lat],
                      $maxDistance: dist
                  }
              },
                  EMongodbCollections.METRO)
              .map((res: IMetroResponse[]) => res[0]);
    }

}


export const geoService: GeoService = new GeoService();