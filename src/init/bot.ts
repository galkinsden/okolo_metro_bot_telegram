/*const Promise = require('bluebird');
Promise.config({
    cancellation: true
});*/
import * as TelegramBot from 'node-telegram-bot-api';
import {config} from '../config'
const token: string = config.telegram.access_token;
export const bot: any = new TelegramBot(token, {polling: true});