import * as Rx from "rxjs/Rx";
import {Subject} from "rxjs/Subject";
import * as moment from 'moment';
import {bot} from './bot';
import {ITelegramMessage, ITelegramMessageLocation} from "../models/telegram-message.model";
import {Subscription} from "rxjs/Subscription";
import {Observable} from "rxjs/Observable";
import {metroService} from "../metro-core/metro-service";
import {IMetroTextImgResponse} from "../metro-core/metro-text-img-response.model";
import {CacheService} from "../utils/cache.service";

export const cacheService: CacheService<number, IMetroTextImgResponse> = new CacheService();

enum EConstTextTypes {
    HELLO_MESSAGE,
    SUBSCRIBE,
    UNSUBSCRIBE,
    WAIT,
    IS_SUBSCRIBE,
    GET_METRO,
    GET_ROUTE
}

const CONST_TEXT: Map<EConstTextTypes, string> = new Map([
    [EConstTextTypes.HELLO_MESSAGE, 'Вы можете узнать нахождение самого ближайшего к Вам метро. Для этого просто отошлите Ваше местоположение'],
    [EConstTextTypes.SUBSCRIBE, 'Хорошо, вы подписались. Вы всегда можете описаться набрав: /Отписаться'],
    [EConstTextTypes.UNSUBSCRIBE, 'Хорошо, вы не подписаны. Вы всегда можете подписаться набрав: /Подписаться'],
    [EConstTextTypes.WAIT, 'Подождите...'],
    [EConstTextTypes.GET_METRO, 'Поиск завершен'],
    [EConstTextTypes.GET_ROUTE, 'Чтобы проложить маршрут, нажмите на ссылку снизу'],
    [EConstTextTypes.IS_SUBSCRIBE, `Подписаться?\\n* Каждое утро в 7:00 Вы будете получать уведомление о погоде. Вы всегда сможете отменить подписку`]
]);

export class Handler {

    public readonly handlerStatusS: Rx.Subject<boolean> = new Subject();

    public constructor() {
        this.init();

    }

    private init(): void {
        this.handlerStatusS.next(true);
        console.log('Handler запущен');
        bot.on('message', this.messageRouter.bind(this));
        //this.initSubscribersRefresh();
    }

/*    /!**
     * Интервал по запуску подписок
     *!/
    private initSubscribersRefresh(): void {
        weatherSubscribeService.getRefreshObservable().subscribe((result: IWeatherSubscribe[]) => {
            result.forEach(element => {
                if (element.cityName) {
                    this.sendWeatherMessageByCity(element.id, element.cityName);
                }
            })

        });
    }*/
/*
    /!**
     * формирование и отправка сообщения по названию города
     * @param {number} chatId
     * @param {string} cityName
     *!/
    private sendMetroByCity(chatId: number, cityName: string): void {
        this.sendMetroMessageAbstract(chatId, this.formWeatherMessageByCity(cityName));
    }*/


    private sendMetroMessageByCoordinates(chatId: number, location: ITelegramMessageLocation, img: boolean = false): void {
        this.sendMetroMessageAbstract(chatId, this.formMetroMessageByCoordinates(location, img), img);
    }


    private sendMetroMessageAbstract(chatId: number, observable: Observable<any>, img: boolean): void {
        this.sendMessage(chatId, CONST_TEXT.get(EConstTextTypes.WAIT), this.clearKeyboard());
        let subscription: Subscription = observable
            .map((res: IMetroTextImgResponse) => {
                if (img && res && !res.error) {
                    this.sendPhoto(chatId, res.html);
                } else {
                    this.sendMessage(chatId, res.html, this.addHtmlParse());
                }
                return res;
            })
            .map((res: IMetroTextImgResponse) => {
                if (res && !res.error) {
                    cacheService.setInCache(chatId,{...res});
                    this.sendRoute(chatId);
                } else {
                    cacheService.deleteFromCache(chatId);
                }
                return res;
            })
           /* .map(res => {
              //  weatherSubscribeService.putSubscribeCity(chatId, res.city);
                return;
            })
            .switchMap(() => {
                return weatherSubscribeService.isAlreadySubscribed(chatId)
            })
            .map(isAlreadySubscribed => {
                console.log('subscribe', isAlreadySubscribed);
                !isAlreadySubscribed && this.sendMessage(chatId, CONST_TEXT.get(EConstTextTypes.IS_SUBSCRIBE), this.addKeyboard([["Да", "Нет"]]));
                return;
            })*/
            .subscribe((res: IMetroTextImgResponse) => {
                this.sendMessage(chatId, CONST_TEXT.get(EConstTextTypes.GET_METRO), this.addKeyboard([[{
                    request_location: true,
                    text: "Узнать ближайшее метро"
                }]]));
                subscription.unsubscribe();
            });
    }

    /**
     * Вступительное сообщение
     * @returns {string}
     */
    private getHelloMessage(): string {
        return `${this.getHelloMessageGreetText()}. ${CONST_TEXT.get(EConstTextTypes.HELLO_MESSAGE)}`
    }

    /**
     * Первая часть вступительного сообщения
     * @returns {string}
     */
    private getHelloMessageGreetText(): string {
        return moment().hour() < 10 && 'Доброе утро' ||
            moment().hour() >= 10 && moment().hour() <= 16 && 'Добрый день' ||
            moment().hour() > 16 && moment().hour() <= 22 && 'Добрый вечер' ||
            moment().hour() > 22 && moment().hour() <= 24 && 'Доброй ночи';
    }

    /**
     * Скрипт работы бота
     * @param {ITelegramMessage} msg
     */
    private messageRouter(msg: ITelegramMessage): void {
        if (msg.text) {
            switch (msg.text) {
                case '/start':
                    this.sendMessage(msg.chat.id, this.getHelloMessage(), this.addKeyboard([[{
                            request_location: true,
                            text: "Узнать ближайшее метро"
                        }], ['Список поддерживаемых городов']]
                    ));
                    break;
                case 'Список поддерживаемых городов':
                    this.sendCityList(msg.chat.id);
                    break;
                case 'Да':
                    /*                case '/Подписаться':
                                    case 'Подписаться':
                                        weatherSubscribeService.putSubscribe(msg.chat.id);
                                        this.sendMessage(msg.chat.id, CONST_TEXT.get(EConstTextTypes.SUBSCRIBE), this.addKeyboard([['Узнать погоду'], ['Отписаться']]));*/
                    break;
                case 'Нет':
                    /* case '/Отписаться':
                     case 'Отписаться':
                         weatherSubscribeService.putUnSubscribe(msg.chat.id);
                         this.sendMessage(msg.chat.id, CONST_TEXT.get(EConstTextTypes.UNSUBSCRIBE), this.addKeyboard([['Узнать погоду'], ['Подписаться']]));*/
                    break;
                default:
                    this.sendMessage(msg.chat.id, this.getHelloMessage(), this.addKeyboard([[{
                            request_location: true,
                            text: "Узнать ближайшее метро"
                        }], ['Список поддерживаемых городов']]
                    ));
                    break;

            }
        }

        if (msg.location) {
            this.sendMetroMessageByCoordinates(msg.chat.id, msg.location, true);
        }

    }

    private formMetroMessageByCoordinates(coordinates: ITelegramMessageLocation, img: boolean = false) {
        return metroService.getMetroByCoordinates(coordinates.latitude, coordinates.longitude, img);
    }

    private sendMessage(chatId: number, message: string, additionalParams?: Object) {
        bot.sendMessage(chatId, message, additionalParams);
    }

    private sendPhoto(chatId: number, photo: string) {
        bot.sendPhoto(chatId, photo);
    }

    private clearKeyboard(object?: Object) {
        if (object) {
            object['reply_markup'] = {hide_keyboard: true};
            return object;
        }
        return {reply_markup: {hide_keyboard: true}}
    }

    private addKeyboard(keyboard: any[][], object?: Object) {

        if (object) {
            object['reply_markup'] = {keyboard: keyboard};
            return object;
        }

        return {reply_markup: {keyboard: keyboard}};
    }

    private addInlineKeyboard(keyboard: any[][], object?: Object) {

        if (object) {
            object['reply_markup'] = {inline_keyboard: keyboard};
            return object;
        }

        return {reply_markup: {inline_keyboard: keyboard}};
    }

    private addHtmlParse(object?: Object) {
        if (object) {
            object['parse_mode'] = 'HTML';
            return object;
        }

        return {parse_mode: 'HTML'};
    }

    private sendRoute(chatId: number) {
        try {
            const reqLng: number = cacheService.getFromCache(chatId).req.coordinates[0];
            const reqLat: number = cacheService.getFromCache(chatId).req.coordinates[1];
            const resLng: number = cacheService.getFromCache(chatId).res.coordinates[0];
            const resLat: number = cacheService.getFromCache(chatId).res.coordinates[1];
            this.sendMessage(chatId, CONST_TEXT.get(EConstTextTypes.GET_ROUTE), this.addInlineKeyboard([[{
                text: 'Построить маршрут до метро',
                url: `https://yandex.ru/maps/213/moscow/?rtext=${reqLat}%2C${reqLng}~${resLat}%2C${resLng}&rtt=pd&ll=${reqLat}%2C${reqLng}&z=11`
            }]]));
            cacheService.deleteFromCache(chatId);
        } catch(err) {
            console.log('Ошибка отправки машрута', err);
        }
    }

    private sendCityList(chatId: number): void {
        const sub: Subscription = metroService
            .getListCityWithMetro()
            .subscribe((res: string[]) => {
                sub.unsubscribe();
                const html: string = res
                    .sort((a: string, b: string) => {
                        if (a.toLocaleLowerCase() < b.toLocaleLowerCase()) return -1;
                        if (a.toLocaleLowerCase() > b.toLocaleLowerCase()) return 1;
                        return 0;
                    })
                    .reduce((acc: string, el: string) => {
                        return `${acc} \n<b>${el}</b>`
                    }, '');
                this.sendMessage(chatId, html, this.addHtmlParse());
            });
    }

}