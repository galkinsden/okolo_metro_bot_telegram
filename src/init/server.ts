import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as Rx from 'rxjs';
import {config} from '../config';
import {Subject} from "rxjs/Subject";
import * as parse from 'ua-parser-js';

export const app = express();
app.use(bodyParser.urlencoded({extended: true}));
//Чтобы hh не блочил ботку)
parse('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36');

export class Server {

    public readonly serverConnectStatusS: Rx.Subject<boolean> = new Subject();

    public constructor() {
        this.init();
    }

    private init(): void {
        app.listen(config.port, () => {
                this.serverConnectStatusS.next(false);
                console.log('Сервер не открыт на порту : ' + config.port);
            },
            () => {
                console.log('Сервер открыт на порту : ' + config.port);
                this.serverConnectStatusS.next(true);
            });
    }
}
