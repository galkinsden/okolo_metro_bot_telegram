import * as Rx from 'rxjs';
import {MongoClient} from 'mongodb';
import {config, EMongodbCollections} from '../config';
import {Subject} from "rxjs/Subject";
import {hhMetroApiService} from "../utils/hh-metro-api.service";
import {Subscription} from "rxjs/Subscription";
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/forkJoin';
import {Observable} from "rxjs/Observable";
import {IHhMetro} from "../models/hh-metro.model";
import {IMetroResponse} from "../metro-core/metro-response.model";
import {Mongodb} from "../utils/mongodb-api";

export class Db {

    public readonly dbConnectStatusS: Rx.Subject<boolean> = new Subject();
    protected static timeToInterval: number = 1000 * 60 * 60 * 24 * 10; //Раз в 10 дней

    public constructor() {
        this.init();
       const subscribeUpdate: Subscription = this.intervalUpdateMetroCollection().subscribe(() => {subscribeUpdate.unsubscribe()});
    }

    private init(): void {
        MongoClient.connect(config.mongodb, (err, database) => {
            if (err) {
                console.error('Ошибка подключения к mongodb!');
                this.dbConnectStatusS.next(false);
                return;
            }
            this.dbConnectStatusS.next(true);
            this.create2dIndex(database);
            database.close();
            console.log(`БД подключена!, ${config.mongodb}`);
        });
    }

    /**
     * Строим 2d индекс в MongoDb для coordinates
     * @param db
     */
    private create2dIndex(db: any): void {
        db.collection(EMongodbCollections.METRO).indexInformation((err,a : any) => {
            if (!a || a && !a.coordinates_2d) {
                db.collection(EMongodbCollections.METRO).createIndex(
                    { coordinates : "2d" }, (err, result) => {
                        console.log('Создание индекса 2d: ', result);
                    });
            } else {
                console.log('2d индекс уже построен');
            }
        });
    }

    /**
     * Загружаем справочники метро с hh
     * @returns {Observable<IMetroResponse[]>}
     */
    private loadMetroCollection(): Observable<IMetroResponse[]> {
        return hhMetroApiService.getMetroJson()
            .map((metroArr: IHhMetro[]) => {
                return metroArr.reduce((acc: IMetroResponse[] ,m: IHhMetro) => {
                    return acc.concat(hhMetroApiService.convertMetro(m))
                }, []);
            })
    }

    /**
     * Обновляем в MongoDb справочники метро с hh
     * @param {IMetroResponse[]} metroResponses
     * @returns {Observable<boolean>}
     */
    private updateMetroCollection(metroResponses: IMetroResponse[]): Observable<boolean> {
        return new Mongodb()
            .remove({}, EMongodbCollections.METRO)
            .switchMap(() => {
                return new Mongodb().insertMany(metroResponses, EMongodbCollections.METRO);
            })
    }

    /**
     * Обновляем справочники MongoDb с определенным интервалом
     * @returns {Observable<boolean>}
     */
    private intervalUpdateMetroCollection(): Observable<boolean> {
        return Observable
            .timer(0, Db.timeToInterval)
            .switchMap(() => this.loadMetroCollection())
            .switchMap((metroResponses: IMetroResponse[]) => this.updateMetroCollection(metroResponses))
    }
}