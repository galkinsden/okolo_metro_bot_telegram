import * as Rx from 'rxjs';
import {Db} from "./db";
import {Server} from "./server";
import {Handler} from "./handler";

export class Init {

    public constructor() {
        this.init();
    }

    private init(): void {

        Rx.Observable.combineLatest(
                new Db().dbConnectStatusS.asObservable(),
                new Server().serverConnectStatusS.asObservable(),
                new Handler().handlerStatusS.asObservable()
            )
            .subscribe(
                ([dbConnectStatusS, serverConnectStatusS, handlerStatusS]: [boolean, boolean, boolean]) => {
                    if (dbConnectStatusS && serverConnectStatusS) {
                        console.log('Телеграм бот успешно запущен!');
                    } else {
                        console.log('Ошибка запуска Телеграм бота!');
                    }
                },
                ([dbConnectStatusS, serverConnectStatusS, handlerStatusS]: [boolean, boolean, boolean]) => {
                    console.log('Ошибка запуска Телеграм бота!');
                });
    }

}