import {IMetroRequest, IMetroResponse} from "./metro-response.model";

export interface IMetroModel {
    error: boolean;
    res: IMetroResponse;
    req: IMetroRequest
}