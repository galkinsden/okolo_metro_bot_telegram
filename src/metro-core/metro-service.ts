import {IMetroModel} from "./metro.model";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import {Subject} from "rxjs/Subject";
import {geoService} from "../utils/geo.service";
import {Subscription} from "rxjs/Subscription";
import {IMetroResponse} from "./metro-response.model";
import {webshotService} from "../utils/webshot.service";
import * as path from 'path';
import {jadeService} from "../utils/jade.service";
import {IMetroTextImgResponse} from "./metro-text-img-response.model";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Mongodb} from "../utils/mongodb-api";
import {EMongodbCollections} from "../config";
import {IHhMetro} from "../models/hh-metro.model";

export class MetroService {

    public getMetroByCoordinates(lat: number, lon: number, img: boolean = false): Observable<Object> {
        return this.getMetroRequestByCoordinates(lat, lon).switchMap((res: IMetroModel) => this.getMetro(res, img))
    }

    private getMetroRequestByCoordinates(lat: number, lon: number): Observable<IMetroModel> {
        let subject: Subject<IMetroModel> = new Subject();
        let subscribe: Subscription = geoService
            .getNearestMetro(lon, lat)
            .subscribe((res: IMetroResponse) => {
                if (res) {
                    subject.next({res : res, req: {coordinates: [lon, lat]}, error: false});
                } else {
                    subject.next({res : res, req: {coordinates: [lon, lat]}, error: true});
                }
                subscribe.unsubscribe();
            });
        return subject.asObservable();
    }

    public getListCityWithMetro(): Observable<string[]> {
        return new Mongodb()
            .find({}, EMongodbCollections.METRO)
            .map((arr: IMetroResponse[]) => {
                return arr.reduce((acc: string[], el: IMetroResponse) => {
                    return !~acc.indexOf(el.nameCity) ? acc.concat(el.nameCity) : acc;
                }, []);
            });
    }

    private getMetro(model: IMetroModel, img: boolean = false): Observable<Object> {
        if (model && model.res && img) {
            return this.getMetroImg(model);
        } else if (model && model.res) {
            return this.getMetroString(model);
        } else {
            return this.getMetroError(model);
        }
    }

    private getMetroImg(model: IMetroModel): Observable<IMetroTextImgResponse> {
        const subject: Subject<IMetroTextImgResponse> = new Subject();
        const subscribe: Subscription = jadeService.getStringFromJade(path.join(__dirname, 'template', 'metro.jade'), {model: model.res})
            .switchMap((html) => {
                return webshotService
                    .getImageByHtml(html);
            })
            .subscribe((img: string) => {
                subject.next({
                    error: false,
                    html: img,
                    res: model.res,
                    req: model.req
                });
                subscribe.unsubscribe();
            });
        return subject.asObservable();
    }

    private getMetroString(model: IMetroModel): Observable<IMetroTextImgResponse> {
        const html: string = `<html><body>Станция: <b>${model.res.name}</b>
                 \nВетка: <b>${model.res.nameLine}</b></body></html>`;
        return new BehaviorSubject<IMetroTextImgResponse>({
            error: false,
            html: html,
            res: model.res,
            req: model.req
        }).asObservable();
    }

    private getMetroError(model: IMetroModel): Observable<IMetroTextImgResponse> {
        return new BehaviorSubject<IMetroTextImgResponse>({
            error: true,
            html: '<b>Информация о ближайшем метро не найдена</b>',
            res: model.res,
            req: model.req
        }).asObservable();
    }
}

export const metroService: MetroService = new MetroService();
