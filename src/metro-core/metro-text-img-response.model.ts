import {IMetroRequest, IMetroResponse} from "./metro-response.model";

export interface IMetroTextImgResponse {
    error: boolean;
    res: IMetroResponse,
    req: IMetroRequest,
    html: string
}