import {ISubscribe, SubscribeFactory} from "../subscribe/subscribe.factory";
import {EMongodbCollections} from "../config";
import {Mongodb} from "../utils/mongodb-api";
import {Observable} from "rxjs/Observable";


export interface IWeatherSubscribe extends ISubscribe {
    cityName?: string;
}

class WeatherSubscribeService extends SubscribeFactory {
     protected subscribes: IWeatherSubscribe[];
     protected timeToInterval: number = 10000;
     protected collectionName: EMongodbCollections = EMongodbCollections.WEATHER_SUBSCRIBER;

     protected formSubscribeObject(subscribers: ISubscribe[]): IWeatherSubscribe[] {
         return this.subscribes.concat(subscribers).filter((element) =>  element.isSubscribe)
     }

    /**
     * Добавление города
     * @param {number} id
     * @param {string} city
     */
     public putSubscribeCity(id: number, city: string): void {
         let subscribeElement: IWeatherSubscribe = this.subscribes.find(el => el.id == id);
         if (!subscribeElement) {
             subscribeElement = {id: id, cityName: city};
             this.subscribes.push(subscribeElement)
         } else {
             subscribeElement.cityName = city;
         }

         this.putToDB(subscribeElement)
     }

    /**
     * Есть ли подписка
     * @param {number} id
     * @returns {Observable<boolean>}
     */
     public isAlreadySubscribed(id: number): Observable<boolean> {

        return new Mongodb().findOne({id: id}, this.collectionName).map((result: IWeatherSubscribe) => {
            return result && result.isSubscribe
        })
     }
}

export const weatherSubscribeService = new WeatherSubscribeService();