export interface IMetroResponse {
    coordinates: ICoordinates;
    name: string,
    id: string,
    nameLine: string,
    colorLine: string,
    idLine: string,
    type: string;
    nameCity: string;
}

export interface IMetroRequest {
    coordinates: ICoordinates;
}

export type ICoordinates = number[]; // [lng, lat];