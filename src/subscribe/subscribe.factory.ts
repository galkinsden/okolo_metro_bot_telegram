import {Observable} from "rxjs/Observable";
import {EMongodbCollections} from "../config";
import {Mongodb} from "../utils/mongodb-api";
import {Subscription} from "rxjs/Subscription";
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';

export interface ISubscribe {
    id: number;
    isSubscribe?: boolean;
}

export class SubscribeFactory {
    protected subscribes: ISubscribe[] = [];
    protected timeToInterval: number = 1000 * 60 * 60 * 24;
    protected collectionName: EMongodbCollections = EMongodbCollections.SUBSCRIBER;
    private refreshObservable: Observable<any>;

    constructor() {
        this.initInterval();
    }

    /**
     * Получение потока для подписки
     */
    public getRefreshObservable(): Observable<any> {
        return this.refreshObservable;
    }

    /**
     * Формируем интервал и объект получаемый от подписчика
     */
    private initInterval(): void {
        this.refreshObservable = Observable
            .interval(this.timeToInterval)
            .switchMap(this.getSubscribersFromDb.bind(this))
            .map(this.formSubscribeObject.bind(this))
    }

    /**
     * Формируем массив id чатов (для переопределения)
     * @returns {number[]}
     */
    protected formSubscribeObject(subscribers: ISubscribe[]): any[] {
        return this.subscribes.concat(subscribers).reduce((result, element) =>  element.isSubscribe ? result.concat(element.id) : result,[])
    }

    /**
     * Создаем подписку
     * @param {number} chatId
     */
    public putSubscribe(chatId: number): void {
        let subscribeElement: ISubscribe = this.subscribes.find(el => el.id == chatId);
        if (!subscribeElement) {
            subscribeElement = {id: chatId, isSubscribe: true};
            this.subscribes.push(subscribeElement)
        } else {
            subscribeElement.isSubscribe = true;
        }

        this.putToDB(subscribeElement)
    }

    /**
     * Отписываем пользователя
     * @param {number} chatId
     */
    public putUnSubscribe(chatId: number): void {
        let subscribeElement: ISubscribe = this.subscribes.find(el => el.id == chatId);
        if (!subscribeElement) {
            subscribeElement = {id: chatId, isSubscribe: false};
            this.subscribes.push(subscribeElement)
        } else {
            subscribeElement.isSubscribe = false;
        }
        this.putToDB(subscribeElement);
    }

    /**
     * Добавление подписки в БД
     * @param {ISubscribe} data
     */
    protected putToDB(data: ISubscribe): void {
        let subscriber: Subscription = new Mongodb().update({id: data.id}, {$set: data}, this.collectionName).subscribe((result) => {

            if(!result) {
                console.error('Подписка не добавлена в бд');
            } else {
                this.subscribes = this.subscribes.filter(element => element.id != data.id);
            }

            subscriber.unsubscribe();
        })
    }

    /**
     * Получение всех данных о подписках
     * @returns {Subscription}
     */
    protected getSubscribersFromDb(): Observable<ISubscribe[]> {
        return new Mongodb().find({}, this.collectionName)
    }

}