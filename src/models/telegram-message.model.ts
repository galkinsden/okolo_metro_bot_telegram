export interface ITelegramMessage {
    message_id: number;
    from: ITelegramMessageFrom;
    chat: ITelegramMessageChat;
    date: number;
    text?: string;
    entities?: ITelegramMessageEntities[];
    photo?: ITelegramMessagePhoto[];
    location?: ITelegramMessageLocation;
}

export interface ITelegramMessageFrom {
    id: number;
    is_bot: boolean;
    first_name: string;
    last_name: string;
    username: string;
    language_code: string;
}

export interface ITelegramMessageChat {
    id: number;
    first_name: string;
    last_name: string;
    username: string;
    type: string;
}

export interface ITelegramMessageEntities {
    offset: number;
    length: number;
    type: string;
}

export interface ITelegramMessagePhoto {
    file_id: string;
    file_size: number;
    width: number;
    height: number;
}

export interface ITelegramMessageLocation {
    latitude: number;
    longitude: number;
}