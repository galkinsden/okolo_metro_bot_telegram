export interface IHhMetro {
    lines: IHhMetroLine[],
    id: string;
    name: string;
}

export interface IHhMetroLine {
    hex_color: string;
    stations: IHhMetroStation[];
    id: string;
    name: string;
}

export interface IHhMetroStation {
    lat: number;
    lng: number;
    order: number;
    id: string;
    name: string;
}