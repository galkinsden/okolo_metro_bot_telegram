import {ITelegramInfo, TelegramInfoModel} from "./telegram-info.model";

export interface ITelegram {
    info: ITelegramInfo;
}

export class TelegramModel {

    public readonly info: TelegramInfoModel = null;

    public constructor(src: ITelegram = null) {
        this.info = src && src.info ? new TelegramInfoModel(src.info) : new TelegramInfoModel(null);
    }
}