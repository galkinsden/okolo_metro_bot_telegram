export enum ETypeEvent {
    test = 'test'
}

export enum ETypeChat {
    BOT = 'BOT',
    MANAGER = 'MANAGER'
}

export interface ITelegramInfo {
    message?: string,
    typeEvent?: ETypeEvent;
    typeChat?: ETypeChat;
    date?: string;
    personId?: string;
}

export class TelegramInfoModel {

    public readonly message: string = '';
    public readonly typeChat: ETypeChat = null;
    public readonly typeEvent: ETypeEvent = null;
    public readonly date: string = '';
    public readonly personId: string = '';

    public constructor(src: ITelegramInfo = null) {
        this.message = src && src.message || '';
        this.typeChat = src && src.typeChat || null;
        this.typeEvent = src && src.typeEvent || null;
        this.date = src && src.date || '';
        this.personId = src && src.personId || '';
    }
}
